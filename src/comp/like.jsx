import React, { useState } from "react";
import axios from "axios";
import "./style.css";
import Comun from "../comun/Comun";

const Like = () => {
  const [email, setEmail] = useState("");
  const [subscribed, setSubscribed] = useState(false);
  const [loadingSubscribe, setLoadingSubscribe] = useState(false);
  const [loadingUnsubscribe, setLoadingUnsubscribe] = useState(false);

  const handleSubscribe = (e) => {
    e.preventDefault();
    setLoadingSubscribe(true);
    if (email === "forbidden@gmail.com") {
      axios
        .post("http://localhost:3000/forbidden")
        .then((response) => {
          console.log(response.data);
        })
        .catch((error) => {
          console.log("error: Email is already in use");
          window.alert("Email is already in use");
        })
        .finally(() => {
          setLoadingSubscribe(false);
        });
    } else {
      axios
        .post("http://localhost:3000/subscribe", { email })
        .then((response) => {
          console.log(response.data);
          setSubscribed(true);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setLoadingSubscribe(false);
        });
    }
  };

  const handleUnsubscribe = (e) => {
    e.preventDefault();
    setLoadingUnsubscribe(true);
    axios
      .post("http://localhost:3000/unsubscribe", { email })
      .then((response) => {
        console.log(response.data);
        setSubscribed(false);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingUnsubscribe(false);
      });
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  return (
    <>
    <div className="my-container">
      <div className="my-header">Join Our Program</div>
      <div className="my-content">
        <div className="my-text">
          Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna
          aliqua.
        </div>
        {subscribed ? (
          <div className="my-input">
            <button
              onClick={handleUnsubscribe}
              disabled={loadingUnsubscribe}
              style={{ opacity: loadingUnsubscribe ? 0.5 : 1 }}
            >
              {loadingUnsubscribe ? "UNSUBSCRIBING" : "UNSUBSCRIBE"}
            </button>
          </div>
        ) : (
          <div className="my-input">
            <div id="rectangle">
              <input
                type="email"
                placeholder="Email"
                id="emailInput"
                value={email}
                onChange={handleEmailChange}
              />
            </div>
            <button
              onClick={handleSubscribe}
              disabled={loadingSubscribe}
              style={{ opacity: loadingSubscribe ? 0.5 : 1 }}
            >
              {loadingSubscribe ? "SUBSCRIBING" : "SUBSCRIBE"}
            </button>
          </div>
        )}
      </div>
    </div>
    <>
    { subscribed && <Comun /> }
    </>
    </>
  );
};



export default Like;

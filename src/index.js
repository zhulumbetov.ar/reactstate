import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Like from './comp/like'; 
import Comun from './comun/Comun'; 


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Like /> 
  </React.StrictMode>
);

reportWebVitals();

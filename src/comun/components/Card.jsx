import React from "react";

import user1 from "../assets/User1.png";
import user2 from "../assets/User2.png";
import user3 from "../assets/User3.png";

const images = {
  1: user1,
  2: user2,
  3: user3,
};

const work = {
  1: "Lead Designer at Company Name",
  2: "Lead Engineer at Company Name",
  3: "CEO at Company Name",
};

const Card = (props) => {
  return (
    <div className="card_container">
      <div className="content">
        <div className="img_box">
          <img src={images[props.id]} alt="" />
        </div>
        <div className="info">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolor.
        </div>
        <div className="name">{props.name}</div>
        <div className="position">{work[props.id]}</div>
      </div>
    </div>
  );
};

export default Card;

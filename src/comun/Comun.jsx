import { useState } from "react";
import "./App.css";
import { useFetch } from "./hooks/useFetch";
import Card from "./components/Card";

function App() {
  const { data, isLoading } = useFetch(
    "http://localhost:3000/community"
  );

  if (isLoading) {
    return <div>Loading...</div>;
  }
  return (
    <section className="main_container">
      <div className="title">Big Community of People Like You </div>
      <div className="additional">
        We’re proud of our products, and we’re really excited when we get
        feedback from our users.
      </div>
      <main className="user_container">
        {data.map((users) => (
          <Card id={users.id} name={users.name} key={users.id}/>
        ))}
      </main>
    </section>
  );
}

export default App;
